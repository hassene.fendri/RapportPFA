\babel@toc {french}{}
\contentsline {chapter}{Liste des Figures}{iv}{Doc-Start}
\contentsline {xchapter}{}{iv}{Doc-Start}
\contentsline {chapter}{R\'esum\'e}{v}{Doc-Start}
\contentsline {chapter}{Introduction G\'en\'erale}{1}{Doc-Start}
\contentsline {chapter}{\numberline {I}Cadre du projet}{2}{chapter.1}
\contentsline {section}{\numberline {1}Pr\'esentation de \IeC {\guillemotleft }VayeTek\IeC {\guillemotright } }{2}{section.1.1}
\contentsline {section}{\numberline {2}M\'ethodologie du travail}{3}{section.1.2}
\contentsline {subsection}{\numberline {2.1}Pr\'esentation de la m\'ethode agile Scrum}{3}{subsection.1.2.1}
\contentsline {subsection}{\numberline {2.2}Les r\^oles Scrum}{3}{subsection.1.2.2}
\contentsline {subsection}{\numberline {2.3}Planning des sprints}{4}{subsection.1.2.3}
\contentsline {chapter}{\numberline {II}\'Etude Pr\'eliminaire}{6}{chapter.2}
\contentsline {section}{\numberline {1}\'Etude de l'existant}{6}{section.2.1}
\contentsline {section}{\numberline {2}Contribution}{7}{section.2.2}
\contentsline {section}{\numberline {3}Sp\'ecification des besoins}{8}{section.2.3}
\contentsline {subsection}{\numberline {3.1}Description des acteurs}{8}{subsection.2.3.1}
\contentsline {subsection}{\numberline {3.2}\'Enum\'eration des Besoins Fonctionnels}{8}{subsection.2.3.2}
\contentsline {subsection}{\numberline {3.3}\'Enum\'eration des Besoins Non-Fonctionnels}{9}{subsection.2.3.3}
\contentsline {chapter}{\numberline {III}Mod\'elisation des besoins et Conception}{11}{chapter.3}
\contentsline {section}{\numberline {1}Mod\'elisation des besoins}{11}{section.3.1}
\contentsline {subsection}{\numberline {1.1}Mod\'elisation des relations entre entit\'es}{11}{subsection.3.1.1}
\contentsline {subsection}{\numberline {1.2}Sc\'enarisation du processus orient\'e \IeC {\guillemotleft } Contribution \IeC {\guillemotright }}{12}{subsection.3.1.2}
\contentsline {section}{\numberline {2}Conception}{13}{section.3.2}
\contentsline {subsection}{\numberline {2.1}Architecture Applicative Fonctionnelle}{13}{subsection.3.2.1}
\contentsline {subsection}{\numberline {2.2}Architecture Logicielle}{14}{subsection.3.2.2}
\contentsline {chapter}{\numberline {IV}R\'ealisation}{16}{chapter.4}
\contentsline {section}{\numberline {1}Architecture applicative technique}{16}{section.4.1}
\contentsline {subsection}{\numberline {1.1}Couche Client et Pr\'esentation : Angular}{17}{subsection.4.1.1}
\contentsline {subsection}{\numberline {1.2}Couche m\'etier : Node.js et Express.js}{18}{subsection.4.1.2}
\contentsline {subsection}{\numberline {1.3}Couche d'int\'egration et Donn\'ees : Mongoose et MongoDB}{20}{subsection.4.1.3}
\contentsline {section}{\numberline {2}Pipeline DevOps}{20}{section.4.2}
\contentsline {subsubsection}{\numberline {2.0.1}M\'ethodologie DevOps}{20}{subsubsection.4.2.0.1}
\contentsline {subsection}{\numberline {2.1}Avantages du DevOps}{21}{subsection.4.2.1}
\contentsline {subsection}{\numberline {2.2}Outils utilis\'es}{22}{subsection.4.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Mocha et Chai}{22}{subsubsection.4.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Git}{22}{subsubsection.4.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}GitLab CI/CD}{23}{subsubsection.4.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}DockerHub}{23}{subsubsection.4.2.2.4}
\contentsline {section}{\numberline {3}Pr\'esentation de l'application}{24}{section.4.3}
\contentsline {subsection}{\numberline {3.1}D\'etention d'un compte MedConf}{24}{subsection.4.3.1}
\contentsline {subsection}{\numberline {3.2}Cr\'eation de congr\`es}{25}{subsection.4.3.2}
\contentsline {subsection}{\numberline {3.3}Liste des congr\`es}{26}{subsection.4.3.3}
\contentsline {subsection}{\numberline {3.4}Validation de l'\'evaluation d'une soumission}{26}{subsection.4.3.4}
\contentsline {chapter}{Conclusion G\'en\'erale et Perspectives}{28}{chapter*.34}
\contentsline {chapter}{Bibliographique}{29}{chapter*.34}
